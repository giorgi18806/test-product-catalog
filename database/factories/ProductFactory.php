<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $product_name = $this->faker->unique()->words($nb=4, $asText=true);
        $category_id = $this->faker->numberBetween(1,3);
        if($category_id == 1) {
            $image = 'sneakers_' . $this->faker->unique()->numberBetween(1, 24) . '.jpg';
        }elseif($category_id == 2) {
            $image = 'monitors_' . $this->faker->unique()->numberBetween(1, 24) . '.jpg';
        }elseif($category_id == 3) {
            $image = 'smartphones_' . $this->faker->unique()->numberBetween(1, 24) . '.jpg';
        }
        return [
            'name' => $product_name,
            'slug' => Str::slug($product_name),
            'description' => $this->faker->text(500),
            'price' => $this->faker->numberBetween(10, 500),
            'stock_status' => 'instock',
            'quantity' => $this->faker->numberBetween(100, 200),
            'image' => $image,
            'category_id' => $category_id,
            'supplier_id' => $this->faker->numberBetween(2,4),
        ];
    }
}
