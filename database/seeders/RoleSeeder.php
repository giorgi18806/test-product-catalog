<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Role::ROLES as $item) {
           $roles[] = [
               'role' => $item,
               'created_at' => now(),
               'updated_at' => now()
           ];
        }

        DB::table('roles')->insert($roles);
    }
}
