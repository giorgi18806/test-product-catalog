<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['Sneakers', 'Monitors', 'Smartphones'];
        foreach($data as $item) {
            $categories[] = [
                'title' => $item,
                'slug' => Str::slug($item),
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        DB::table('categories')->insert($categories);
    }
}
