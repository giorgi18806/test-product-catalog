<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'first_name' => 'Admin',
                'email' => 'admin@admin.com',
                'role_id' => '1',
                'password' => bcrypt('password'),
            ],
            [
                'first_name' => 'First',
                'last_name' => 'Supplier',
                'email' => 'supplier_1@supplier.com',
                'role_id' => '2',
                'markup' => 10,
                'password' => bcrypt('123456789'),
            ],
            [
                'first_name' => 'Second',
                'last_name' => 'Supplier',
                'email' => 'supplier_2@supplier.com',
                'role_id' => '2',
                'markup' => 7,
                'password' => bcrypt('123456789'),
            ],
            [
                'first_name' => 'Third',
                'last_name' => 'Supplier',
                'email' => 'supplier_3@supplier.com',
                'role_id' => '2',
                'markup' => 12,
                'password' => bcrypt('123456789'),
            ],
            [
                'first_name' => 'First',
                'last_name' => 'User',
                'email' => 'user@user.com',
                'role_id' => '3',
                'password' => bcrypt('123456789'),
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
