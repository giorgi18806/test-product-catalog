<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BuyerController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => 'role:admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
        Route::resource('/category', CategoryController::class);
        Route::resource('/user', UserController::class);
    });
    Route::group(['middleware' => 'role:supplier', 'prefix' => 'supplier', 'as' => 'supplier.'], function () {
        Route::get('/dashboard', [SupplierController::class, 'dashboard'])->name('dashboard');
        Route::get('/my-product', [Suppliercontroller::class, 'my_products'])->name('my-products');
        Route::get('/add-product', [ProductController::class, 'index'])->name('add-product');
        Route::post('/add-product/{product}', [SupplierController::class, 'add_product'])->name('add-new-price');
        Route::get('/profile/{user}', [UserController::class, 'show'])->name('profile');
        Route::get('/profile/{user}/edit', [SupplierController::class, 'edit'])->name('profile.edit');
        Route::put('/profile/{user}', [SupplierController::class, 'update'])->name('profile.update');
    });
    Route::group(['middleware' => 'role:buyer', 'prefix' => 'buyer', 'as' => 'buyer.'], function () {
        Route::get('/dashboard', [BuyerController::class, 'dashboard'])->name('dashboard');
    });
    Route::group(['middleware' => 'role:admin|supplier'], function () {
        Route::resource('/product', ProductController::class);
    });

});
//Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home');
//Route::get('home', [HomeController::class, 'index'])->name('home');
