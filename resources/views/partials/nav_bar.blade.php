<li class="nav-item">
    <a href="{{ route(Auth::user()->role->role . '.dashboard') }}" class="nav-link">Home</a>
</li>
@can('admin-supplier', Auth::user())
    <li class="nav-item">
        <a href="{{ route('product.index') }}" class="nav-link">Products</a>
    </li>
@endcan
@can('admin-only', Auth::user())
    <li class="nav-item">
        <a href="{{ route('admin.category.index') }}" class="nav-link">Categories</a>
    </li>
    <li class="nav-item">
        <a href="/" class="nav-link">Product Attributes</a>
    </li>
    <li class="nav-item">
        <a href="/" class="nav-link">Supplier Product</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.user.index') }}" class="nav-link">Users</a>
    </li>
@endcan
@can('supplier-only', Auth::user())
    <li class="nav-item">
        <a href="{{ route('supplier.my-products') }}" class="nav-link">My Products</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('supplier.add-product') }}" class="nav-link">Add Product</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('supplier.profile', Auth::user()) }}" class="nav-link">My Profile</a>
    </li>
@endcan
