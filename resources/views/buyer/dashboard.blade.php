@extends('layouts.app')

@section('content')
    <style>
        nav svg {
            height: 20px;
        }
        nav .hidden {
            display: block !important;
        }
        p.text-sm {
            margin-top: 10px;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Products table') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Category</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>

                                    <td>{{ $product->id }}</td>
                                    <td>
                                        <a href="{{ route('product.show', $product->slug) }}">
                                            <img src="{{ asset('images/products')}}/{{ $product->image }}" alt="{{ $product->name }}" width="60">
                                        </a>
                                    </td>
                                    <td><a href="{{ route('product.show', $product->slug) }}" style="text-decoration: none;color: #000">{{ $product->name }}</a></td>
                                    <td>{{ $product->getMarkupPrice() }}</td>
                                    <td>{{ $product->category->title }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="justify-content-center">{{ $products->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
