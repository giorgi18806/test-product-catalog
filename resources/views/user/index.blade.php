@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Users Table') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href="{{ route('admin.user.create') }}" class="btn btn-success mb-3">Create New User</a>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Markup</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->full_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->role }}</td>
                                    <td>{{ isset($user->markup) ? $user->markup . '%' : ''}}</td>
                                    <td>
                                        <a class="btn btn-success btn-sm" href="{{ route('admin.user.edit', $user->id) }}"><i
                                                class="fa fa-pencil-square-o"></i></a>

                                        <form action="{{ route('admin.user.destroy', $user) }}" method="post"
                                              id="deleteForm-{{ $user->id }}" style="display: none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <a class="btn btn-danger btn-sm" href="" onclick="
                                            if(confirm('Are you sure you really want to delete this user?')){
                                            event.preventDefault();
                                            document.getElementById('deleteForm-{{ $user->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }"><i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
