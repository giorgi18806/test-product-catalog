@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit User Data') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form role="form" action="{{ route('supplier.profile.update', $user) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">First Name</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Enter user first name" class="form-control input-md" name="first_name" value="{{ $user->first_name }}">
                                    @error('first_name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">Last Name</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Enter user last name" class="form-control input-md" name="last_name" value="{{ $user->last_name }}">
                                    @error('last_name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">User Email</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="email" placeholder="Enter user email" class="form-control input-md" name="email" value="{{ $user->email }}">
                                    @error('email') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">Change Password</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Enter new password" class="form-control" name="password" autocomplete="off" value="{{ old('password') }}">
                                    @error('password') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3"></label>
                                <div class="col-md-4 offset-md-3">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="{{ route('supplier.profile', $user) }}" class="btn btn-secondary">Back</a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
