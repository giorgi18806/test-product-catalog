@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create New User') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form role="form" action="{{ route('admin.user.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">First Name</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Enter user first name" class="form-control input-md" name="first_name" value="{{ old('first_name') }}">
                                    @error('first_name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">Last Name</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Enter user last name" class="form-control input-md" name="last_name" value="{{ old('last_name') }}">
                                    @error('last_name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">User Email</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="email" placeholder="Enter user email" class="form-control input-md" name="email" value="{{ old('email') }}">
                                    @error('email') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">User Role</label>
                                <div class="col-md-6 offset-md-3">
                                    <select name="role_id" id="role_id" class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if($role->id == 3) selected @endif>{{ $role->role }}</option>
                                        @endforeach
                                    </select>
                                    @error('role_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">Markup</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="number" placeholder="Enter a percentage markup" class="form-control" name="markup" value="{{ old('markup') }}">
                                    @error('markup') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3"></label>
                                <div class="col-md-4 offset-md-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('admin.user.index') }}" class="btn btn-secondary">Back</a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
