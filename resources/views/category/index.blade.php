@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Categories Table') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href="{{ route('admin.category.create') }}" class="btn btn-success mb-3">Create New Category</a>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Category Name</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->title }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>
                                        <a class="btn btn-success btn-sm" href="{{ route('admin.category.edit', $category->slug) }}"><i
                                                class="fa fa-pencil-square-o"></i></a>

                                        <form action="{{ route('admin.category.destroy', $category) }}" method="post"
                                              id="deleteForm-{{ $category->id }}" style="display: none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <a class="btn btn-danger btn-sm" href="" onclick="
                                            if(confirm('Are you sure you really want to delete this category?')){
                                            event.preventDefault();
                                            document.getElementById('deleteForm-{{ $category->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }"><i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
