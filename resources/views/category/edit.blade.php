@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Update Category') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form role="form" action="{{ route('admin.category.update', $category->slug) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3">Category Name</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Enter category name" class="form-control input-md" name="title" value="{{ $category->title }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label offset-md-3"></label>
                                <div class="col-md-4 offset-md-3">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
