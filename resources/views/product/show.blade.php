@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-between">
                            <h3 class="ml-5">{{ __($product->name) }}</h3>

                            <a href="{{ route('product.index') }}" class="btn btn-outline-dark mr-5">Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="wrap-product-detail">
                            <div class="detail-media">
                                <div class="product-gallery">
                                    <ul class="slides">
                                        <li data-thumb="{{ asset('images/products') }}/{{ $product->image }}">
                                            <img src="{{ asset('images/products') }}/{{ $product->image }}" alt="{{ $product->name }}" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="detail-info">
                                <div class="row justify-content-around">
                                    <div class="product-rating">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <a href="#" class="count-review">(05 review)</a>
                                    </div>
                                    <div class="stock-info in-stock">
                                        <p class="availability">Availability:
                                            <span class="{{ $product->stock_status == 'instock' ? 'text-success' : 'text-danger' }}"><b>{{ $product->stock_status }}</b></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="row justify-content-around">
                                    <h2 class="product-name">{{ $product->name }}</h2>
                                    <div class="float-right">
                                        <h1 class="text-primary font-weight-bold" >${{ $product->price }}</h1>
                                    </div>
                                </div>
                                <div class="mx-3">
                                    {{ $product->description }}
                                </div>
                                @can('supplier-only', Auth::user())
                                    <div class="wrap-butons my-5">
                                        <a href="{{ route('supplier.add-new-price', $product) }}" class="btn btn-outline-dark">Add New Price</a>
                                    </div>
                                @endcan
                                @can('user', Auth::user())
                                    <div class="wrap-butons my-5">
                                        <a href="#" class="btn btn-outline-dark">Add to Wishlist</a>
                                    </div>
                                @endcan
                            </div>
                                <table class="shop_attributes">
                                    <tbody>
                                    <tr>
                                        <th>@if($product->category_id != 1) Screen @endif Size</th><td class="product_dimensions">12 x 15 x 23 cm</td>
                                    </tr>
                                    <tr>
                                        <th>Color</th><td>Black, Blue, Grey, Violet, Yellow</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
