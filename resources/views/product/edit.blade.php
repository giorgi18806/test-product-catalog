@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit Product') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form role="form" action="{{ route('product.update', $product) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Product Name</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Product Name" class="form-control input-md" name="name" value="{{ $product->name }}">
                                    @error('name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Description</label>
                                <div class="col-md-6 offset-md-3">
                                    <textarea placeholder="Description" class="form-control" name="description">{{ $product->description }}</textarea>
                                    @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Price</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Regular Price" class="form-control input-md" name="price" value="{{ $product->price }}">
                                    @error('price') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Stock</label>
                                <div class="col-md-6 offset-md-3">
                                    <select class="form-control" name="stock_status">
                                        <option value="instock" @if($product->stock_status == 'instock') selected @endif>InStock</option>
                                        <option value="outofstock" @if($product->stock_status == 'outofstock') selected @endif>Out of Stock</option>
                                    </select>
                                    @error('stock_status') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Quantity</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="text" placeholder="Quantity" class="form-control input-md" name="quantity" value="{{ $product->quantity }}">
                                    @error('quantity') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Product Image</label>
                                <div class="col-md-6 offset-md-3">
                                    <input type="file" class="input-file" name="image" value="{{ $product->image }}">
                                    {{--                                    @if(isset($image))--}}
                                    {{--                                        <img src="{{ $image->temporaryUrl() }}" width="120">--}}
                                    {{--                                    @endif--}}
                                    @error('image') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Category</label>
                                <div class="col-md-6 offset-md-3">
                                    <select class="form-control" name="category_id">
                                        <option value="" selected disabled>Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" @if($product->category_id == $category->id) selected @endif>{{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3">Supplier</label>
                                <div class="col-md-6 offset-md-3">
                                    <select class="form-control" name="supplier_id">
                                        <option value="" selected disabled>Select Supplier</option>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{ $supplier->id }}" @if($product->supplier_id == $supplier->id) selected @endif>{{ $supplier->full_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('supplier_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label offset-md-3"></label>
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="{{ route('product.index') }}" class="btn btn-secondary">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
