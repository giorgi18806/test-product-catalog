@extends('layouts.app')

@section('content')
    <style>
        nav svg {
            height: 20px;
        }
        nav .hidden {
            display: block !important;
        }
        p.text-sm {
            margin-top: 10px;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Products table') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @can('admin-only', Auth::user())
                            <a href="{{ route('product.create') }}" class="btn btn-success mb-3">Create New Product</a>
                        @endcan
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>

                                    <td>{{ $product->id }}</td>
                                    <td>
                                        <a href="{{ route('product.show', $product->slug) }}">
                                            <img src="{{ asset('images/products')}}/{{ $product->image }}" alt="{{ $product->name }}" width="60">
                                        </a>
                                    </td>
                                    <td><a href="{{ route('product.show', $product->slug) }}" style="text-decoration: none;color: #000">{{ $product->name }}</a></td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->category->title }}</td>
                                    <td>
                                        <a class="btn btn-success btn-sm" href="{{ route('product.edit', $product->slug) }}" style="padding: 5px 4px !important;"><i
                                                class="fa fa-pencil-square-o"></i></a>

                                        <form action="{{ route('product.destroy', $product) }}" method="post"
                                              id="deleteForm-{{ $product->id }}" style="display: none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <a class="btn btn-danger btn-sm" href="" onclick="
                                            if(confirm('Are you sure you really want to delete this product?')){
                                            event.preventDefault();
                                            document.getElementById('deleteForm-{{ $product->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }" style="padding: 5px 5px"><i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                            <div class="justify-content-center">{{ $products->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
