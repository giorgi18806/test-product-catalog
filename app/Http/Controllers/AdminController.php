<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{
    public function dashboard()
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        return view('admin.dashboard');
    }
}
