<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplierUpdateRequest;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SupplierController extends Controller
{
    public function dashboard()
    {
        if(! Gate::allows('supplier-only')) {
            return abort(401);
        }
        return view('supplier.dashboard');
    }

    public function my_products()
    {
        if(! Gate::allows('supplier-only')) {
            return abort(401);
        }
        $products = Product::where('supplier_id', auth()->id())->paginate(10);
        return view('product.index', compact('products'));
    }

    public function add_product(Product $product)
    {
        if(! Gate::allows('supplier-only')) {
            return abort(401);
        }
          return "Under construction";
//        return redirect()->route('product.index')->with('status', 'New price has been added successfully');
    }

    public function edit(User $user)
    {
        if(! Gate::allows('supplier-only')) {
            return abort(401);
        }
        return view('user.edit-supplier', compact('user'));
    }

    public function update(SupplierUpdateRequest $request, User $user )
    {
        if(! Gate::allows('supplier-only')) {
            return abort(401);
        }
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['email'] = $request->email;
        if($request->password != null) {
            $data['password'] = bcrypt($request->password);
        }
        $user->update($data);
        return redirect()->route('supplier.profile', $user)->with('status', 'User has been updated successfully');
    }
}
