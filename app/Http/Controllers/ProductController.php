<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $data = $request->validated();
        $data['slug'] = Str::slug($data['name']);
        $data['image'] = time() . '-' . $data['slug'] . '.' . $request->image->extension();
        $request->image->move(public_path('images/products'), $data['image']);
        Product::create($data);
        return redirect()->route('product.index')->with('status', 'Product has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param $product_slug
     * @return Application|Factory|View
     */
    public function show($product_slug)
    {
        $product = Product::where('slug', $product_slug)->first();
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $product_slug
     * @return Application|Factory|View
     */
    public function edit($product_slug)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $product = Product::where('slug', $product_slug)->first();
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function update(ProductRequest $request, Product $product)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $data = $request->validated();
        $data['slug'] = Str::slug($data['name']);
        if(request()->has('image')){
            $data['image'] = time() . '-' . $data['slug'] . '.' . $request->image->extension();
            $request->image->move(public_path('images/products'), $data['image']);
        }
        $product->update($data);
        return redirect()->route('product.index')->with('status', 'Product has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return RedirectResponse
     */
    public function destroy(Product $product)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $product->delete();
        return redirect()->back()->with('status', 'Product has been deleted successfully');
    }
}
