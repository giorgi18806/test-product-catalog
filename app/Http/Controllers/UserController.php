<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{

    public function index()
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $users = User::all();
        return view('user.index', compact('users'));
    }

    public function create()
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $data = $request->validated();
        $data['password'] = bcrypt('123456789');
        User::create($data);
        return redirect()->route('admin.user.index')->with('status', 'User has been created successfully');
    }

    public function show(User $user)
    {
        if(! Gate::allows('supplier-only')) {
            return abort(401);
        }
        return view('user.show', compact('user'));
    }

    public function edit(User $user)
    {
//        if(! Gate::allows('admin-only')) {
//            return abort(401);
//        }
        return view('user.edit', compact('user'));
    }

    public function update(UserUpdateRequest $request, User $user )
    {
        return $request;
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $data = $request->validated();
        $user->update($data);
        return redirect()->route('admin.user.index')->with('status', 'User has been updated successfully');
    }

    public function destroy (User $user)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $user->delete();
        return redirect()->back()->with('status', 'User has been deleted successfully');
    }
}
