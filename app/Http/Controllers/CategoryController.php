<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $categories = Category::all();
        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request
     * @return RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $data = $request->validated();
        $data['slug'] = Str::slug($data['title']);
        Category::create($data);
        return redirect()->route('admin.category.index')->with('status', 'Category has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $category_slug
     * @return Application|Factory|View
     */
    public function edit($category_slug)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $category = Category::where('slug', $category_slug)->first();
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryRequest $request
     * @param $category_slug
     * @return RedirectResponse
     */
    public function update(CategoryRequest $request, $category_slug)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $category = Category::where('slug', $category_slug)->first();
        $data = $request->validated();
        $data['slug'] = Str::slug($data['title']);
        $category->update($data);
        return redirect()->route('admin.category.index')->with('status', 'Category has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return RedirectResponse
     */
    public function destroy(Category $category)
    {
        if(! Gate::allows('admin-only')) {
            return abort(401);
        }
        $category->delete();
        return redirect()->back()->with('status', 'Category has been deleted successfully');
    }
}
