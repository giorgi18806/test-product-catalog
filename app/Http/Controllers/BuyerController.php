<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class BuyerController extends Controller
{
    public function dashboard()
    {
        $products = Product::paginate(10);
        return view('buyer.dashboard', compact('products'));
    }
}
