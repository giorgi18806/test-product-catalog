<?php

namespace App\Providers;

use App\Http\Requests\UserRequest;
use App\Models\Category;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([ 'user.create', 'user.edit'], function($view) {
            $view->with('roles', Role::all());
        });
        View::composer([ 'product.create', 'product.edit'], function($view) {
            $view->with([
                'categories' => Category::all(),
                'suppliers' => User::where('role_id', 2)->get(),
            ]);
        });
    }
}
